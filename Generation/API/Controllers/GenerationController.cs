﻿using Generation;
using Generation.Abstraction;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api-world-generation/[controller]")]
    public class GenerationController : ControllerBase
    {
        private readonly IGenerator generator;

        public GenerationController(IGenerator _generator)
        {
            generator = _generator;
        }

        [HttpGet]
        public IActionResult Generate([FromQuery] GenerationParameters parameters)
        {
            World world = generator.Generate(parameters);
            generator.Populate(world, parameters);

            return Ok(world);
        }
    }
}
