﻿using System.Security.Cryptography.X509Certificates;
using DotnetNoise;
using Generation.Abstraction;
using Generation.Utils;

namespace Generation.Implementation;

public class WorldGenerator : IGenerator
{
    private FastNoise Noise { get; set; }

    public WorldGenerator()
    {
        Noise = new FastNoise();
    }

    public World Generate(GenerationParameters parameters)
    {
        var world = new World(parameters.Size, parameters.Size);

        if (parameters.NewSeed) Noise = new FastNoise(Random.Shared.Next());

        Noise.Frequency = parameters.Frequency;
        Noise.Lacunarity = parameters.Lacunarity;
        Noise.Octaves = parameters.OctaveCount;
        Noise.Gain = parameters.Gain;
        Noise.UsedNoiseType = parameters.NoiseType;

        for (var i = 0; i < parameters.Size; i++)
            for (var j = 0; j < parameters.Size; j++)
                world.SetCell(i, j, Noise.GetNoise(i, j) * parameters.Scale + parameters.Delta);

        return world;
    }

    public World Populate(World world, GenerationParameters parameters)
    {
        int numberOfVillagedGenerated = 0;
        int iterations = 0;
        int villageSize = (int)(parameters.VillagesSpread * parameters.VillagesPopulation);

        List<(int x, int y)> houses = new();

        while (numberOfVillagedGenerated < parameters.NumberOfVillages && iterations < 100)
        {
            var space = FindSuitableSpace(world, villageSize, villageSize);

            if (space.HasValue)
            {
                BuildVillage(world, parameters.VillagesPopulation, space, villageSize, parameters.HousesSize, houses);
                numberOfVillagedGenerated++;
            }
            iterations++;
        }


        return world;
    }

    private static void BuildVillage(World world, float villagesPopulation, (int x, int y)? space, int villageSize, int housesSize, List<(int x, int y)> houses)
    {
        for (int v = 0; v < villagesPopulation; v++)
        {
            var house = FindSuitableHouse(houses, space.Value.x, space.Value.y, villageSize, housesSize);
            if (house.HasValue) 
                BuildHouse(world, house.Value.x, house.Value.y, housesSize);
        }
    }

    private static void BuildHouse(World world, int x, int y, int housesSize)
    {
        for (int w = 0; w < housesSize; w++)
            for (int h = 0; h < housesSize; h++)
                world.SetCell(x + w, y + h, CellType.House);
    }

    private static (int x, int y)? FindSuitableHouse(List<(int x, int y)> houses, int valueX, int valueY, int villageSize, int houseSize)
    {
        int iteration = 0;
        while (iteration < 100)
        {
            int houseX = Random.Shared.Next(valueX, valueX + villageSize);
            int houseY = Random.Shared.Next(valueY, valueY + villageSize);

            if (!houses.Exists(h => Util.RectangleOverlap(houseX, houseY, h.x, h.y, houseSize)))
            {
                houses.Add((houseX, houseY));
                return (houseX, houseY);
            }

            iteration++;
        }
        return null;
    }

    private static (int x, int y)? FindSuitableSpace(World world, int width, int height)
    {
        int iteration = 0;

        while (iteration < 1000)
        {
            int x = Random.Shared.Next(world.Width);
            int y = Random.Shared.Next(world.Height);

            CellType topLeft = world.GetCell(x, y);
            CellType bottomLeft = world.GetCell(x, y + height);
            CellType topRight = world.GetCell(x + width, y);
            CellType bottomRight = world.GetCell(x + width, y + height);

            if (topLeft.IsSuitable() && topRight.IsSuitable() && bottomLeft.IsSuitable() && bottomRight.IsSuitable())
                return (x, y);

            iteration++;
        }

        return null;
    }
}