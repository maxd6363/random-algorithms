﻿using DotnetNoise;

namespace Generation
{
    public class GenerationParameters
    {
        public int Size { get; set; }
        public float Scale { get; set; }
        public bool NewSeed { get; set; }
        public float Frequency { get; set; }
        public int OctaveCount { get; set; }
        public float Persistence { get; set; }
        public float Lacunarity { get; set; }
        public float Gain { get; set; }
        public float Delta { get; set; }
        public FastNoise.NoiseType NoiseType { get; set; }
        public int NumberOfVillages { get; set; }
        public int HousesSize { get; set; }
        public float VillagesPopulation { get; set; }
        public int VillagesSpread { get; set; }
    }
}
