import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { GeneratorService } from './generator.service';
import { palette } from './model/palette';
import { Settings } from './model/settings';
const PixelArt = require('pixel-art');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  canvas!: HTMLCanvasElement | null;
  scale: number = 6;

  title = 'world-generation';

  constructor(private generator: GeneratorService) { }

  settingsChanged(settings: Settings) {
    this.generateCanvas(settings);
  }

  generateCanvas(settings: Settings) {
    this.generator.get(settings)
      .subscribe(world => {
        this.canvas!.width = world.width * this.scale;
        this.canvas!.height = world.height * this.scale;

        PixelArt.art(world.data)
          .palette(palette)
          .scale(this.scale)
          .draw(this.canvas!.getContext('2d'));
      });
  }

  ngAfterViewInit(): void {
    this.canvas = document.getElementById("main-canvas") as HTMLCanvasElement;
  }
}
