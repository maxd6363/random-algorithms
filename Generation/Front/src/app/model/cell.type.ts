export enum CellType {
  None = 0,
  DeepWater = 1,
  Water = 2,
  Shore = 3,
  Beach = 4,
  Grass = 5,
  Hill = 6,
  Mountain = 7,
  ExtremeMountain = 8
}
