import { NoiseType } from "../model/noise.type";
import { Preset } from "../model/preset";

export let CustomPreset: Preset = {
  name: "Custom", settings: {
    scale: 1,
    frequency: 0.02,
    gain: 2,
    delta: 0,
    octaveCount: 3,
    persistence: 0.5,
    lacunarity: 2,
    newSeed: true,
    noiseType: NoiseType.PerlinFractal,
    size: 192,
    numberOfVillages: 0,
    villagesPopulation: 0,
    housesSize: 0,
    villagesSpread: 1
  }
};


export const Presets: Preset[] = [
  CustomPreset,
  {
    name: "Normal", settings: {
      scale: 1,
      frequency: 0.015,
      gain: 2,
      delta: -0.209,
      octaveCount: 3,
      persistence: 0.34,
      lacunarity: 1.6,
      newSeed: true,
      noiseType: NoiseType.PerlinFractal,
      size: 256,
      numberOfVillages: 0,
      villagesPopulation: 0,
      housesSize: 0,
      villagesSpread: 1
    }
  }, {
    name: "Isles", settings: {
      scale: 1,
      frequency: 0.015,
      gain: 1.6,
      delta: -0.6,
      octaveCount: 3,
      persistence: 0.34,
      lacunarity: 1.6,
      newSeed: true,
      noiseType: NoiseType.PerlinFractal,
      size: 256,
      numberOfVillages: 0,
      villagesPopulation: 0,
      housesSize: 0,
      villagesSpread: 1
    }
  }, {
    name: "Village", settings: {
      scale: 1,
      frequency: 0.01,
      gain: 0.07,
      delta: 0,
      octaveCount: 2,
      persistence: 0.34,
      lacunarity: 1.6,
      newSeed: true,
      noiseType: NoiseType.PerlinFractal,
      size: 256,
      numberOfVillages: 2,
      villagesPopulation: 10,
      housesSize: 5,
      villagesSpread: 6
    }
  }, {
    name: "Isles Populated", settings: {
      scale: 1,
      frequency: 0.015,
      gain: 1.6,
      delta: -0.6,
      octaveCount: 3,
      persistence: 0.34,
      lacunarity: 1.6,
      newSeed: true,
      noiseType: NoiseType.PerlinFractal,
      size: 256,
      numberOfVillages: 3,
      villagesPopulation: 2,
      housesSize: 4,
      villagesSpread: 5
    }
  },
]
