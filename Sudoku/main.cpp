#include <iostream>
#include <cstdio>

#include "sudoku.hpp"

using namespace std;

int main() {
    // 0 si case vide
    int exemple[9][9] = {
        {0, 0, 0, 0, 7, 2, 1, 0, 5},
        {2, 5, 7, 0, 0, 0, 0, 4, 6},
        {0, 1, 0, 5, 0, 9, 0, 0, 8},
        {0, 0, 2, 1, 5, 7, 0, 0, 4},
        {7, 4, 0, 0, 9, 0, 8, 0, 0},
        {1, 9, 0, 0, 8, 4, 0, 0, 7},
        {0, 2, 9, 0, 0, 0, 5, 0, 0},
        {0, 7, 8, 4, 1, 5, 0, 6, 0},
        {0, 6, 1, 0, 0, 0, 0, 0, 0}
    };

    Sudoku * sudoku = new Sudoku();
    for (int row=0; row<9; row++) {
        for (int col=0; col<9; col++) {
            sudoku->grille[row][col] = exemple[row][col];
        }
    }

    if (sudoku->solveSudoku(0, 0)) {
        cout << "Solved Sudoku: " << endl;
        for (int row = 0; row < sudoku->taille; row++) {
            for (int col = 0; col < sudoku->taille; col++) {
                cout << sudoku->grille[row][col] << " ";
            }
            cout << endl;
        }
    } 
    else {
        cout << "This Sudoku cannot be solved." << endl;
    }
    return 0;
}