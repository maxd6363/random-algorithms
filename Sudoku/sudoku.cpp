#include "sudoku.hpp"

Sudoku::Sudoku() : taille(9) {
    this->grille = new int*[9];
    for (int i=0; i<9; i++) {
        this->grille[i] = new int[9];
    }
}

/** méthode permettant de check si un chiffre est possible pour une case précise dans une ligne
 * entrée : un numéro de ligne, un chiffre
 * sortie : un booléen, vrai si chiffre possible, faux sinon
*/
bool Sudoku::checkRow(int row, int num) {
    for (int col = 0; col < this->taille; col++) {
        if (this->grille[row][col] == num)
            return true;
    }
    return false;
}

/** méthode permettant de check si un chiffre est possible pour une case précise dans une colonne
 * entrée : un numéro de colonne, un chiffre
 * sortie : un booléen, vrai si chiffre possible, faux sinon
*/
bool Sudoku::checkCol(int col, int num) {
    for (int row = 0; row < this->taille; row++) {
        if (this->grille[row][col] == num)
            return true;
    }
    return false;
}

/** méthode permettant de check si un chiffre est possible pour une case précise dans une grille
 * entrée : un numéro de ligne, un numéro de colonne, un chiffre
 * sortie : un booléen, vrai si chiffre possible, faux sinon
*/
bool Sudoku::checkGrid(int startRow, int startCol, int num) {
    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            if (this->grille[row + startRow][col + startCol] == num)
                return true;
        }
    }
    return false;
}

/** méthode permettant de check si un chiffre est possible pour une case précise selon la ligne, la colonne et la grille où elle se trouve
 * entrée : un numéro de ligne, un numéro de colonne, un chiffre
 * sortie : un booléen, vrai si chiffre possible, faux sinon
*/
bool Sudoku::isSafe(int row, int col, int num) {
    return !this->checkRow(row, num) && !this->checkCol(col, num) && !this->checkGrid(row - row % 3, col - col % 3, num);
}

/** méthode récursive appliquant le principe du backtracking pour résoudre un sudoku 
 * entrée : un numéro de ligne, un numéro de colonne
 * sortie : un booléen, vrai si solution trouvée, faux sinon
*/
bool Sudoku::solveSudoku(int row, int col) {
    // si on est arrivé après la dernière case, on sort de la méthode
    if (row == this->taille - 1 && col == this->taille)
        return true;

    // si on arrive après la dernière case de la ligne, on change de ligne
    if (col == this->taille) {
        row++;
        col = 0;
    }

    // si la case courrante contient déjà une valeur, on passe à la suivante
    if (this->grille[row][col] != 0)
        return this->solveSudoku(row, col + 1);

    // dans tous les autres cas (ie la case courrante est vide)
    // on teste les chiffres de 1 à 9
    for (int num = 1; num <= this->taille; num++) {
        // on regarde si le chiffre n'est pas déjà présent dans la ligne, colonne ou grille
        if (isSafe(row, col, num)) {
            this->grille[row][col] = num;
            // puis on passe à la case suivante
            if (this->solveSudoku(row, col + 1))
                return true;
            // dans le cas où l'on serait sorti de la méthode car on a pas trouvé de chiffre,
            // on revient en arrière en mettant la case à vide, c'est la méthode du backtracking
            this->grille[row][col] = 0;
        }
    }
    // dans le cas où on a pas trouvé de chiffre disponible, on sort de la méthode
    return false;
}