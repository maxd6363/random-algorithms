import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-case',
  templateUrl: './case.component.html',
  styleUrls: ['./case.component.css']
})
export class CaseComponent {
  @Output() stateChange = new EventEmitter<boolean>();

  state: boolean = false;

  @Input() rows: number = 0;
  @Input() cols: number = 0;

  @HostListener("click") onClick(){
    this.toggle();
  }

  toggle() {
    this.state = !this.state;
    this.stateChange.emit(this.state);
  }

}
