import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import Point from './model/point';
import Rectangle from './model/rectangle';

@Injectable({
  providedIn: 'root'
})
export class RectangleService {
  constructor(private client: HttpClient) { }

  process(points: Point[]): Observable<Rectangle[]> {
    return this.client.post<Rectangle[]>(`${environment.api}Rectangle`, points);
  }
}
