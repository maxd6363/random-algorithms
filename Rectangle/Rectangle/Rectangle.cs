﻿namespace RectangleLib
{
    public struct Rectangle
    {
        public Point? Point1 { get; set; }
        public Point? Point2 { get; set; }
        public Point? Point3 { get; set; }
        public Point? Point4 { get; set; }

        public bool IsValid => Point1 != null && Point2 != null && Point3 != null && Point4 != null;

        public override string ToString()
        {
            return $"{Point1}, {Point2}, {Point3}, {Point4}";
        }
    }
}
