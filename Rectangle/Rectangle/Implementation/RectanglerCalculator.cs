using RectangleLib.Abstraction;

namespace RectangleLib.Implementation
{
    public class RectanglerCalculator : IRectangleCalculator
    {
        /// <summary>
        /// Algorithme pour calculer les rectangles présents à partir d'une liste de point.
        /// Le principe est de calculer la longeur et le centre entre tous les points, deux à deux.
        /// Si la longeur et le centre sont les mêmes, on a obligatoirement un rectangle entre les 4 points.
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public ICollection<Rectangle> Process(ICollection<Point> points)
        {
            // Si on a moins de 4 points, pas la peine de calculer.
            if (points.Count is 0 or < 4) return new List<Rectangle>();

            // On initialise notre structure de données
            //  en clé : la distance entre les deux points et le centre
            //  en valeur : les points concernés
            var distances = new Dictionary<(Point, int), Rectangle>();

            // On boucle sur tous les points
            for (int i = 0; i < points.Count; i++)
            {
                // Sur tous les autres points restants
                for (int j = 0; j < i; j++)
                {
                    var first = points.ElementAt(i);
                    var second = points.ElementAt(j);

                    // On calcule le centre entre les deux points
                    var center = new Point
                    {
                        X = first.X + second.X,
                        Y = first.Y + second.Y
                    };

                    // On calcule la distance entre les deux centres, on laisse au carré pour ne pas avoir à calculer la racine carrée  
                    var distance = (first.X - second.X) * (first.X - second.X) +
                                   (first.Y - second.Y) * (first.Y - second.Y);
                    
                    // Si on a trouvé un couple de point avec le même centre et la même distance, on complète la ligne pour créer notre rectangle.
                    if (distances.TryGetValue((center, distance), out var value))
                    {
                        value.Point3 = first;
                        value.Point4 = second;
                        distances[(center, distance)] = value;
                    }
                    // Sinon on ajoute le centre et la distance au dictionnaire avec les deux points que l'on est en train de traiter
                    else
                    {
                        distances.Add((center, distance), new Rectangle
                        {
                            Point1 = first,
                            Point2 = second,
                            Point3 = null,
                            Point4 = null
                        });
                    }
                }
            }

            // On retourne la liste des rectangles qui possèdent bien 4 points
            return distances.Values.Where(x => x.IsValid).ToList();
        }
    }
}
