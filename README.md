# AAC

## Maxime POULAIN

### Rectangle

Une démo interactive est disponible à l'adresse suivante : [virtualdrops.cf/rectangle-calculator/](http://virtualdrops.cf/rectangle-calculator/)

> Le IP peut être bloqué par l'école, utiliser un réseau de type 4G en cas d'échec.
> Mon petit serveur peut possiblement être down de temps en temps, ne pas hésiter à me contacter au cas où le site serait inaccessible.

<img src="https://i.ibb.co/tCQTRg0/image-2023-01-21-225725074.png">

### Génération procédurale


Une démo interactive est disponible à l'adresse suivante : [virtualdrops.cf/world-generation/](http://virtualdrops.cf/world-generation/)


## Coralie PIERRE :

### Sudoku

### Trouver le plus grand rectangle dans une grille
